#!/usr/bin/python3
# -*- coding: utf-8 -*-


__prog__ = "subtrs"
__author__ = "dslackw"
__copyright__ = 2022
__version_info__ = (2, 3)
__version__ = "{0}.{1}".format(*__version_info__)
__license__ = "MIT"
__email__ = "d.zlatanidis@gmail.com"
__website__ = "https://gitlab.com/dslackw/subtrs"

#!/usr/bin/python3
# -*- coding: utf-8 -*-


def usage(status):
    """ CLI --help menu. """
    cli_help: str = (
        "Usage: subtrs [subtitles_file] [destination languages]\n\n"
        "       Simple tool that translates video subtitles\n"
        "       Support subtitles files [*.sbv, *.vtt, *.srt]\n"
        "       Destination languages [en,de,ru] etc.\n\n"
        "Optional arguments:\n"
        "       --color      View translated text with colour.\n"
        "       --progress   Show progress bar.\n"
        "       --export     Export the text only.\n"
        "  -l,  --languages  Show all supported languages.\n"
        "  -v,  --version    Print the version and exit.\n"
        "  -h,  --help       Show this message and exit."
        )

    print(cli_help)
    raise SystemExit(status)

subtrs (video subtitles translator)
==========================================
Version 2.3 - (07/12/2022)
### Updated
- Code style

Version 2.2 - (28/05/2022)
### Added
- export text option

Version 2.1 - (25/03/2022)
### Fixed
- Merge job title and progress bar

Version 2.0 - (23/03/2022)
### Fixed
- Pypi classifiers for license

Version 1.9 - (23/03/2022)
### Updated
- License switch to MIT
- code style

Version 1.8 - (20/03/2022)
### Added
- job title
### Updated
- progress bar

Version 1.7 - (19/03/2022)
### Added
- rename --silent with --progress bar
- added progress bar

Version 1.6 - (19/03/2022)
### Added
- auto detect the language used

Version 1.5 - (19/03/2022)
### Updated
- cli menu

Version 1.4 - (19/03/2022)
### Fixed
- setup.py file version

Version 1.3 - (19/03/2022)
### Added
- Menu that show supported languages

Version 1.2 - (18/03/2022)
### Added
- Support export miltiple subtitles files

Version 1.1 - (18/03/2022)
### Updated
- Improve stability

Version 1.0 - (17/03/2022)
### Release
- first release!
